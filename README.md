# Weather Forecast
A simple project to show the current weather around of the world.

## Available Scripts

In the project directory, you can run:

### `npm install`

### `npm start` or `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
