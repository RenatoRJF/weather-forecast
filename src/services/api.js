import axios from 'axios'

export const apiKey = '0b8e90389b31271791b3bc6211360a29';

const api = axios.create({
  baseURL: `https://api.openweathermap.org/data/2.5`
});

export default api;
