const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const weekDays = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
]

export const showIcon = item => require(`../assets/img/${item[0].icon}.png`);

export const showMonth = item => months[new Date(item.dt * 1000).getUTCMonth()];

export const showWeekDay = item => weekDays[new Date(item.dt * 1000).getDay()];

export const formatTemp = temp => temp ? Math.trunc(temp) : null;
