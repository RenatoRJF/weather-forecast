import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import { showWeekDay, formatTemp, showIcon } from '../../services/util';
import './styles.scss';

const WeatherDays = props => {

  const allDayWeather = item => (
    <p key={item.dt}>
      <strong>{item.dt_txt.substr(11, 5)}</strong>
      <span>{formatTemp(item.main.temp)} °C</span>
    </p>
  );

  const [days] = useState(props.days);

  return (
    <div className="weather-days">
      <TransitionGroup component={null}>
        {days.map((day, i) => (
          <CSSTransition
            key={day.id}
            in={true}
            appear={true}
            timeout={i * 60}
            classNames="item">

            <div key={day.id} className="day">
              <h4>{showWeekDay(day)}</h4>

              <div className='temp'>
                <span className="min">{formatTemp(day.main.temp_max)} °C</span>
                <span className="max">{formatTemp(day.main.temp_min)} °C</span>
              </div>

              <div className="all-temps">
                {day[day.id].map(temp => allDayWeather(temp))}
              </div>

              <img src={showIcon(day.weather)} alt="weather" />

              <p className="conditions">{day.weather[0].description}</p>
            </div>
          </CSSTransition>
        ))}
      </TransitionGroup>
    </div>
  )
}

WeatherDays.propTypes = {
  days: PropTypes.array.isRequired,
};

export default WeatherDays;
