import React from 'react';
import PropTypes from 'prop-types';
import WeatherDays from '../WeatherDays';

import { showIcon, formatTemp } from '../../services/util';

import './styles.scss';

const Weather = props => {
  const { forecast, cityWeather } = props.data;
  const { name, sys, main, weather } = cityWeather.data;
  const { list= [] } = forecast.data;

  const listGroup = list.reduce((acc, cur) => {
    let dt_txt = cur.dt_txt.substr(0, 10);
    return (
      acc.filter(item => item.hasOwnProperty(dt_txt)).length
      ? acc
      : [
          ...acc,
          {
            ...cur,
            id: dt_txt,
            [dt_txt]: list.filter(item => item.dt_txt.substr(0, 10) === dt_txt)
          }
        ]
    );
  }, []);

  return (
    <div className="weather">
      <header>
        <div>
          <span className="temperature">{formatTemp(main.temp)} °C</span>

          <h1 className="city">{name}, {sys.country}</h1>

          <p className="max-min">
            <span>Max: {formatTemp(main.temp_max)} °C </span>
            <span>Min: {formatTemp(main.temp_min)} °C </span>
          </p>
        </div>

        <ul className="details">
          <li>
            <img src={showIcon(weather)} alt="weather" />
            <span>{weather[0].description}</span>
          </li>
          <li><strong>Humidity:</strong> {main.pressure}%</li>
          <li><strong>Pressure:</strong> {main.pressure} MB</li>
        </ul>
      </header>

      <WeatherDays days={listGroup} />
    </div>
  )
}

Weather.propTypes = {
  data: PropTypes.shape({
    forecast: PropTypes.object.isRequired,
    cityWeather: PropTypes.object.isRequired,
  }).isRequired,
};

export default Weather;
