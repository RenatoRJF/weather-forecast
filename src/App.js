import React, { Component } from 'react';
import Autocomplete from 'react-autocomplete';
import { CSSTransition } from 'react-transition-group';

import Weather from './components/Weather'

import api, { apiKey } from './services/api';

import './styles.scss';

class App extends Component {
  state = {
    places: [
      { id: 1, city: 'Lisbon', country: 'Portugal' },
      { id: 2, city: 'London', country: 'England' },
      { id: 3, city: 'Madrid', country: 'Barcelona' },
      { id: 4, city: 'Amsterdam', country: 'Netherlands' },
      { id: 5, city: 'Boston', country: 'USA' },
      { id: 6, city: 'São Paulo', country: 'Brazil' },
      { id: 7, city: 'Sofia', country: 'Bulgaria' }
    ],
    value: '',
    weatherData: null,
    dataLoaded: false
  }

  matchPlaceToTerm(place, value) {
    return (
      place.city.toLowerCase().includes(value.toLowerCase()) ||
      place.country.toLowerCase().includes(value.toLowerCase())
    )
  }

  async loadCityWeather(value) {
    this.setState({ value, weatherData: null, dataLoaded: true });

    const forecast = await api.get(`forecast?q=${value}&appid=${apiKey}&units=metric`);
    const cityWeather = await api.get(`weather?q=${value}&appid=${apiKey}&units=metric`);

    this.setState({ weatherData: { forecast, cityWeather } });
  }

  render() {
    const { places, value, weatherData } = this.state;
    const animation = {
      timeout: 1000,
      appear: true,
      in: true,
    }

    const centerContentClass = () => !this.state.dataLoaded ? 'center-content': '';

    return (
      <div className={`App ${centerContentClass()}`}>
        <CSSTransition {...animation} classNames="header">
          <header className="main-header">
            <h1>Weather Forecast</h1>
          </header>
        </CSSTransition>

        <CSSTransition  {...animation} classNames="auto-complete">
          <div className="wrapper-autocomplete">
            <Autocomplete
              getItemValue={item => item.city}
              items={places}
              renderItem={(item, isHighlighted) =>
                <div
                  key={item.id}
                  className={`item ${isHighlighted ? 'item-highlighted' : ''}`}>

                  {item.city}, {item.country}
                </div>
              }
              value={value}
              shouldItemRender={this.matchPlaceToTerm}
              onChange={event => this.setState({ value: event.target.value })}
              onSelect={this.loadCityWeather.bind(this)}
              renderInput={props => <input {...props} placeholder="e.g. London" />}
            />
          </div>
        </CSSTransition>

        {weatherData && <Weather data={weatherData} />}
      </div>
    );
  }
}

export default App;
